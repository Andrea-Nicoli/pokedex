import UIKit

class PokeDetailViewController: UIViewController {
    @IBOutlet private weak var imgView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var pI: UIActivityIndicatorView!
    
    var img: UIImage?
    var pokemon: Pokemon?
    
    let max_stat_value = 190
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if pokemon != nil {
            stackView.isHidden = false
            label.isHidden = true
            pI.startAnimating()
        }

        fetchImageByPokemonImageUrl(pokemon?.sprites.other.officialArtwork.front_default, completion: { [weak self] image in
            self?.imgView.image = image
            self?.pI.stopAnimating()
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if let statsViews = stackView.subviews as? [StatView] {
            for i in 0...statsViews.count-1 {
                let statView = statsViews[i]
                if let statValue = pokemon?.stats[i].base_stat,let statName = self.pokemon?.stats[i].stat?.name  {
                    statView.valueLbl.text = "\(statValue)"
                    statView.nameLbl.text = "\(statName)"
                    statView.progressView.setProgress(Float(statValue) / Float(max_stat_value), animated:true)
                }
            }
        }
    }
}
