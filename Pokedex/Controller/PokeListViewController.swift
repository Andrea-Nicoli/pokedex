import UIKit

class PokeListViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout{
   
    var collapseDetailViewController: Bool = true
    
    var pokedexResults:[PokedexResult] = []
    var pokemonDict:[Int:Pokemon] = [:]
    var imgDict:[Int:UIImage] = [:]

    let itemsLimitperFetch = 200
    let itemsNotShowedAfterNewFetch = 20 //to fetch new pokemon on the list before finish scrolling
    
    override func viewDidLoad() {
        collectionView.register(UINib(nibName: "PokeCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PokeCollectionCell")
        super.viewDidLoad()
        
        fectchPokedex()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let navigationController = segue.destination as? UINavigationController, let viewController = navigationController.topViewController as? PokeDetailViewController
        else {
            fatalError()
        }
        
        viewController.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem
        viewController.navigationItem.leftItemsSupplementBackButton = true
        
        if let indexPath = collectionView.indexPathsForSelectedItems?.first {
            if let p = pokemonDict[indexPath.item] {
                viewController.pokemon = p
                viewController.title = pokedexResults[indexPath.item].name?.capitalized
                viewController.img = imgDict[indexPath.item]
            }
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pokedexResults.count
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        print("indexPath.row: \( indexPath.row) pokedexResults.count: \(pokedexResults.count)")

        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PokeCollectionCell", for: indexPath) as? PokeCollectionCell {
            if let name = pokedexResults[indexPath.item].name?.capitalized {
                cell.nameLbl.text = "\(indexPath.item + 1). " + name
            }
            
            if let img = imgDict[indexPath.item] {
                cell.imgView.image = img
            }
            
            if pokedexResults.count - itemsNotShowedAfterNewFetch == indexPath.row {
                fectchPokedex()
            }

            downloadDataForPokemon(id:indexPath.item)

            return cell
        }
        return UICollectionViewCell()
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collapseDetailViewController = false

        performSegue(withIdentifier: "detailSegue", sender: nil)
    }

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? PokeCollectionCell, let img = self.imgDict[indexPath.item] {
            fillCellWithImg(cell:cell,img:img)
        }
    }
    
    private func fillCellWithImg(cell: PokeCollectionCell, img: UIImage?) {
        cell.imgView.image = img
        cell.pI.stopAnimating()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize( width: self.view.frame.size.width/2,height:self.view.frame.size.width/2)
    }
    
    func fectchPokedex() {
        print("fectchPokedex")
        fetchPokedex(offset: pokedexResults.count, limit: itemsLimitperFetch, completion: { [weak self] pokedex in
            self?.pokedexResults.append(contentsOf: pokedex.results)
            DispatchQueue.main.async {
                var indexes = [IndexPath]()
                for n in 0...pokedex.results.count-1 {
                    indexes.append(IndexPath(row: n, section: 0))
                }
                self?.collectionView?.insertItems(at: indexes)
            }
        })
    }
    
    func downloadDataForPokemon(id:Int) {
        if pokemonDict[id] == nil {
            fetchPokemonByUrl(pokedexResults[id].url, completion: { [weak self] pokemon in
                self?.pokemonDict[id] = pokemon
                fetchImageByPokemonImageUrl(pokemon.sprites.front_shiny/*other.officialArtwork.front_default*/, completion: { [weak self] image in
                    self?.imgDict[id] = image
                    if let visibleIndexPaths = self?.collectionView.indexPathsForVisibleItems {
                        let indexes = visibleIndexPaths.map({ $0.item })
                        if indexes.contains(id) {
                            if let cell = self?.collectionView.cellForItem(at: IndexPath(item: id, section: 0)) as? PokeCollectionCell {
                                self?.fillCellWithImg(cell:cell, img:image)
                            }
                        }
                    }
                })
            })
        }
    }
}

