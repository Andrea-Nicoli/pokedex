//
//  Service.swift
//  Pokedex
//
//  Created by Andrea Nicoli on 17/01/2021.
//

import UIKit

func fetchPokedex(offset:Int,limit:Int, completion: @escaping (Pokedex) -> ()) {
    let urlString = "https://pokeapi.co/api/v2/pokemon?limit=\(limit)&offset=\(offset)"
    guard let url = URL(string: urlString) else { return }
    
    URLSession.shared.dataTask(with: url) {(data, response, error) in
        guard let data = data else { return }
        
        do {
            let pokedex = try JSONDecoder().decode(Pokedex.self, from: data)
            completion(pokedex)
        } catch let jsonErr{
            print("Error serializing JSON: ", jsonErr)
        }
    }.resume()
}

func fetchPokemonByUrl(_ urlString:String?, completion: @escaping (Pokemon) -> ()) {
    guard let urlString = urlString, let url = URL(string: urlString) else { return }
    
    URLSession.shared.dataTask(with: url) {(data, response, error) in
        guard let data = data else { return }
        
        do {
            let pokemon = try JSONDecoder().decode(Pokemon.self, from: data)
            completion(pokemon)
        } catch let jsonErr {
            print("Error serializing inner JSON:", jsonErr)
        }
    }.resume()
}

func fetchImageByPokemonImageUrl(_ urlString:String?, completion: @escaping (UIImage?) -> ()) {
    guard let urlString = urlString, let url = URL(string: urlString) else { return }
    DispatchQueue.global().async {
        if let data = try? Data(contentsOf: url) {
            DispatchQueue.main.async {
                completion(UIImage(data: data))
            }
        }
    }
}
