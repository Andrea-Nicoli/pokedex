import UIKit

class PokeCollectionCell:UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var pI: UIActivityIndicatorView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imgView?.image = UIImage(named: "pokeball")
        pI.startAnimating()
    }
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor.init(white: 0, alpha:0.15) : UIColor.clear
        }
    }
}
