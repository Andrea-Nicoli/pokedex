import UIKit

//POKEDEX LIST
struct Pokedex: Decodable {
    let results: [PokedexResult]
}

struct PokedexResult: Decodable {
    let name: String?
    let url: String?
}


//POKEMON
struct Pokemon : Decodable {
    let sprites: Sprites
    let stats:[Stats]
    let types:[Types]
}

///stat
struct Stats : Decodable{
    let base_stat:Int?
    let stat:Stat?

}
struct Stat : Decodable{
    let name:String?
}

///type
struct Types : Decodable{
    let type:Type?
}
struct Type : Decodable{
    let name:String?
}

///images
struct Sprites : Decodable {
    let other: Others
    let front_shiny: String?
}
struct Others : Decodable {
    let officialArtwork: OfficialArtwork
    
    //for dash char in json
    enum CodingKeys: String, CodingKey {
        case officialArtwork = "official-artwork"
    }
}
struct OfficialArtwork : Decodable {
    let front_default: String?
}




