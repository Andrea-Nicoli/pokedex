//
//  AppDelegate.swift
//  Pokedex
//
//  Created by Andrea Nicoli on 15/01/2021.
//

import UIKit

@main

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var splitViewDelegate = SplitViewDelegate()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if let splitViewController = self.window?.rootViewController as? UISplitViewController, let navigationController = splitViewController.viewControllers.last as? UINavigationController {
            splitViewController.delegate = splitViewDelegate
            if UIDevice.current.userInterfaceIdiom == .pad { splitViewController.preferredDisplayMode = .allVisible } //detail will fit into right

            navigationController.topViewController?.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem
            navigationController.topViewController?.navigationItem.leftItemsSupplementBackButton = true
        }
        
        return true
    }
}

